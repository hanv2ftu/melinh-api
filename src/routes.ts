import { Routes } from 'nest-router';
import { CmsModule } from './cms/cms.module';
import { MobileAppModule } from "./mobile-app/mobile-app.module";
import { AuthDashboardModule } from './routes/cms/auth-dashboard/auth-dashboard.module';
import { FlowerDashboardModule } from './routes/cms/flower-dashboard/flower-dashboard.module';
import { FlowerTypeModule } from './routes/cms/flower-type/flower-type.module';
import { OrderDashboardModule } from './routes/cms/order-dashboard/order-dashboard.module';
import { AddressModule } from './routes/mobile/address/address.module';
import { AuthMobileModule } from './routes/mobile/auth-mobile/auth-mobile.module';
import { FlowerMobileModule } from './routes/mobile/flower-mobile/flower-mobile.module';
import { NotificationModule } from './routes/mobile/notification/notification.module';
import { OrderMobileModule } from './routes/mobile/order-mobile/order-mobile.module';
import { PromotionModule } from './routes/mobile/promotion/promotion.module';
import { ProvinceModule } from './routes/mobile/province/province.module';

export const routes: Routes = [
    {
        path: '/mobile',
        module: MobileAppModule,
        children: [
            { path: '/', module: AuthMobileModule },
            { path: '/', module: AddressModule },
            { path: '/', module: FlowerMobileModule },
            { path: '/', module: OrderMobileModule },
            { path: '/', module: ProvinceModule },
            { path: '/', module: NotificationModule },
            { path: '/', module: PromotionModule },
            
        ]
    },
    {
        path: '/cms',
        module: CmsModule,
        children: [
            { path: '/', module: AuthDashboardModule },
            { path: '/', module: FlowerDashboardModule },
            { path: '/', module: OrderDashboardModule },
            { path: '/', module: FlowerTypeModule },
            
        ],
    }
   

];
