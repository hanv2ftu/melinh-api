import { Module } from '@nestjs/common';
import { DatabaseModule } from 'src/common/database/database.module';
import { FlowerMobileController } from './flower-mobile.controller';
import { FlowerMobileProviders } from './flower-mobile.providers';
import { FlowerMobileService } from './flower-mobile.service';

@Module({
    imports: [DatabaseModule],
    controllers: [FlowerMobileController],
    providers: [...FlowerMobileProviders,FlowerMobileService],
    exports:[FlowerMobileService]
})
export class FlowerMobileModule {}
