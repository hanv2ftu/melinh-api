import { Inject, Injectable } from '@nestjs/common';
import { Constants } from 'src/common/constants';
import { DeleteResult, getManager, Repository } from 'typeorm';
import { Flower } from './entities/flower.entity';
import { v4 as uuidv4 } from 'uuid';
import { GetListFlowerDto } from './dto/get-list-flower.dto';
import { GetDiscountFlowerDto } from './dto/get-discount-flower.dto';
import { GetHotFlowerDto } from './dto/get-hot-flower.dto';
import { Order } from '../order-mobile/entities/order.entity';
import { GetHistoryFlowerDto } from './dto/get-history-flower.dto';
import { GetActiveFlowerDto } from './dto/get-active-flower.dto';

@Injectable()
export class FlowerMobileService {
  constructor(@Inject(Constants.flowerMobileReposistory)
  private flowerRepository: Repository<Flower>) {

  }

 

  async findAll(getListFlowerDto: GetListFlowerDto): Promise<any> {
    let category = getListFlowerDto.category;
    const manager = getManager();
    const totalDetails = await manager.createQueryBuilder(Flower, "flower")
      .select("count(flower.id) as total")
      .where("flower.category=:category", { category: category })
      .andWhere("flower.status=:status", { status: 'ACTIVE' })
      .getRawOne();
    const list = await manager.createQueryBuilder(Flower, "flower")
      .select("flower.id", "id")
      .addSelect("flower.title", "title")
      .addSelect("flower.avaUrl", "avaUrl")
      .addSelect("flower.imageList", "imageList")
      .addSelect("flower.currentPrice", "currentPrice")
      .addSelect("flower.oldPrice", "oldPrice")
      .addSelect("flower.category", "category")
      .addSelect("flower.description", "description")
      .addSelect("flower.status", "status")
      .where("flower.category=:category", { category: category })
      .andWhere("flower.status=:status", { status: 'ACTIVE' })
      .getRawMany();
    return { total: parseInt(totalDetails.total), list: list };
  }

  async findActiveAll(getActiveDto: GetActiveFlowerDto): Promise<any> {
    const manager = getManager();
    const totalDetails = await manager.createQueryBuilder(Flower, "flower")
      .select("count(flower.id) as total")
      .andWhere("flower.status=:status", { status: 'ACTIVE' })
      .getRawOne();
    const list = await manager.createQueryBuilder(Flower, "flower")
      .select("flower.id", "id")
      .addSelect("flower.title", "title")
      .addSelect("flower.avaUrl", "avaUrl")
      .addSelect("flower.imageList", "imageList")
      .addSelect("flower.currentPrice", "currentPrice")
      .addSelect("flower.oldPrice", "oldPrice")
      .addSelect("flower.category", "category")
      .addSelect("flower.description", "description")
      .addSelect("flower.status", "status")
      .addSelect(("((flower.oldPrice-flower.currentPrice)/flower.oldPrice)*100"), "discountPercent")
      .where("flower.status=:status", { status: 'ACTIVE' })
      .getRawMany();
    return { total: parseInt(totalDetails.total), list: list };
  }


  async getDiscountList(getDiscountrDto: GetDiscountFlowerDto): Promise<any> {
    const manager = getManager();
    const totalDetails = 10;
    const list = await manager.createQueryBuilder(Flower, "flower")
      .select("flower.id", "id")
      .addSelect("flower.title", "title")
      .addSelect("flower.avaUrl", "avaUrl")
      .addSelect("flower.imageList", "imageList")
      .addSelect("flower.currentPrice", "currentPrice")
      .addSelect("flower.oldPrice", "oldPrice")
      .addSelect(("((flower.oldPrice-flower.currentPrice)/flower.oldPrice)*100"), "discountPercent")
      .addSelect("flower.category", "category")
      .addSelect("flower.description", "description")
      .addSelect("flower.status", "status")
      .andWhere("flower.status=:status", { status: 'ACTIVE' })
      .orderBy('discountPercent', 'DESC')
      .limit(10)
      .offset(0)
      .getRawMany();
    return { total: totalDetails, list: list };
  }

  async getHotList(getHotFlowerDto: GetHotFlowerDto): Promise<any> {
    const manager = getManager();
    const totalDetails = 10;
    const list = await manager.createQueryBuilder(Flower, "flower")
      .select("flower.id", "id")
      .addSelect("flower.title", "title")
      .addSelect("flower.avaUrl", "avaUrl")
      .addSelect("flower.imageList", "imageList")
      .addSelect("flower.currentPrice", "currentPrice")
      .addSelect("flower.oldPrice", "oldPrice")
      .addSelect(("((flower.oldPrice-flower.currentPrice)/flower.oldPrice)*100"), "discountPercent")
      .addSelect("flower.category", "category")
      .addSelect("flower.description", "description")
      .addSelect("SUM(order.quantity)", "totalOrder")
      .leftJoin(Order, "order", "flower.id=order.flowerId")
      .where("flower.status=:status", { status: 'ACTIVE' })
      .groupBy("flower.id")
      .addGroupBy("flower.title")
      .addGroupBy("flower.avaUrl")
      .orderBy('totalOrder', 'DESC')
      .limit(10)
      .offset(0)
      .getRawMany();
    return { total: totalDetails, list: list };
  }

  async getHistoryList(getHistoryFlowerDto: GetHistoryFlowerDto,user): Promise<any> {
    const manager = getManager();
    const totalDetails = await manager.createQueryBuilder(Flower, "flower")
    .select("count(flower.id) as total")
    .leftJoin(Order, "order", "flower.id=order.flowerId")
    .where("flower.status=:status", { status: 'ACTIVE' })
    .andWhere("order.userId=:userId", { userId: user.userId })
    .getRawOne();
    const list = await manager.createQueryBuilder(Flower, "flower")
      .select("flower.id", "id")
      .addSelect("flower.title", "title")
      .addSelect("flower.avaUrl", "avaUrl")
      .addSelect("flower.imageList", "imageList")
      .addSelect("flower.currentPrice", "currentPrice")
      .addSelect("flower.oldPrice", "oldPrice")
      .addSelect(("((flower.oldPrice-flower.currentPrice)/flower.oldPrice)*100"), "discountPercent")
      .addSelect("flower.category", "category")
      .addSelect("flower.description", "description")
      .leftJoin(Order, "order", "flower.id=order.flowerId")
      .where("flower.status=:status", { status: 'ACTIVE' })
      .andWhere("order.userId=:userId", { userId: user.userId })
      .getRawMany();
    return { total: totalDetails, list: list };
  }

  async findOne(id: string): Promise<Flower> {
    return await this.flowerRepository.findOne(id);
  }

  

  async remove(id: string): Promise<DeleteResult> {
    return await this.flowerRepository.delete(id);
  }
}
