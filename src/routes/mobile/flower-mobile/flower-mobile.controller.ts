import { Controller, Get, Post, Body, Put, Param, Delete, UseGuards, Req, Query, HttpCode, HttpStatus } from '@nestjs/common';
import { ApiHeader, ApiOkResponse, ApiTags } from '@nestjs/swagger';
import { JwtAuthGuard } from 'src/common/jwt/jwt-auth.guard';
import {  FlowerMobileService } from './flower-mobile.service';
import { GetListFlowerDto } from './dto/get-list-flower.dto';
import { GetDiscountFlowerDto } from './dto/get-discount-flower.dto';
import { GetHotFlowerDto } from './dto/get-hot-flower.dto';
import { GetHistoryFlowerDto } from './dto/get-history-flower.dto';
import { GetActiveFlowerDto } from './dto/get-active-flower.dto';




//Add resource tag for Swagger
@ApiTags('flower')
@Controller('flower')
export class FlowerMobileController {
  constructor(private readonly flowerService: FlowerMobileService) {}

  @HttpCode(HttpStatus.OK)
  @ApiOkResponse({ description: 'Successfully getlist' })
  @Post('getlistActive')
  findActiveAll(@Req() request: any, @Body() getActiveDto: GetActiveFlowerDto) {
    return this.flowerService.findActiveAll(getActiveDto);
  }

  @HttpCode(HttpStatus.OK)
  @ApiOkResponse({ description: 'Successfully getlist' })
  @Post('getlist')
  findAll(@Req() request: any, @Body() getListFlowerDto: GetListFlowerDto) {
    return this.flowerService.findAll(getListFlowerDto);
  }

  @HttpCode(HttpStatus.OK)
  @ApiOkResponse({ description: 'Successfully getDiscountList' })
  @Post('getDiscountList')
  getDiscountList(@Req() request: any, @Body() getDiscountDto: GetDiscountFlowerDto) {
    return this.flowerService.getDiscountList(getDiscountDto);
  }

  @HttpCode(HttpStatus.OK)
  @ApiOkResponse({ description: 'Successfully getHotList' })
  @Post('getHotList')
  getHotList(@Req() request: any, @Body() gethotDto: GetHotFlowerDto) {
    return this.flowerService.getHotList(gethotDto);
  }

  @ApiHeader({
    name: 'Authorization',
    description: 'Access token',
  })
  @HttpCode(HttpStatus.OK)
  @UseGuards(JwtAuthGuard)
  @ApiOkResponse({ description: 'Successfully getHistoryList' })
  @Post('getHistoryList')
  getHistoryList(@Req() request: any, @Body() getHistoryDto: GetHistoryFlowerDto) {
    return this.flowerService.getHistoryList(getHistoryDto,request.user);
  }

  @HttpCode(HttpStatus.OK)
  @ApiOkResponse({ description: 'Successfully getdetail' })
  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.flowerService.findOne(id);
  }


  
}
