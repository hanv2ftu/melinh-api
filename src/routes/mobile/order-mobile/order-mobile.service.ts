import { Inject, Injectable } from '@nestjs/common';
import { Constants } from 'src/common/constants';
import { DeleteResult, getManager, Repository } from 'typeorm';
import { Order } from './entities/order.entity';
import { v4 as uuidv4 } from 'uuid';
import { CreateOrderDto } from './dto/create-order.dto';
import { MakeOrderDto } from './dto/make-order.dto';


@Injectable()
export class OrderMobileService {
  constructor(@Inject(Constants.orderMobileReposistory)
  private flowerRepository: Repository<Order>) {

  }
  async create(makeOrderDto: MakeOrderDto,user): Promise<Order> {
    let array: CreateOrderDto[] = makeOrderDto.orderList;
    let orderArray = array.map((ele:CreateOrderDto)=>{
      return {
        id:uuidv4(),
        userId:user.userId,
        phone:user.phone,
        addressId:makeOrderDto.addressId,
        status:"NEW",
        deliveryDate:makeOrderDto.deliveryDate,
        flowerId:ele.flowerId,
        quantity:ele.quantity,
        createdDate:this.getDate(),
        createdBy:user.fullName,
        updatedDate: this.getDate(),
        updatedBy:user.fullName
      }
    })
    const manager = getManager();
    var query = "INSERT INTO melinh.Order(id, flowerId, userId, phone, addressId, quantity, status, deliveryDate, createdDate, createdBy, updatedDate, updatedBy) VALUES "
    orderArray.forEach((c, idx) => {
      query += "('" + c.id + "','" + c.flowerId + "','" + c.userId + "','" + c.phone + "','" + c.addressId + "','" + c.quantity + "','" + c.status + "','" + c.deliveryDate + "','" + c.createdDate + "','" + c.createdBy + "','" + c.updatedDate + "','" + c.updatedBy + "')";
      if (idx < orderArray.length - 1) {
        query += " , "
      }
    });
    console.log(query);
    return await manager.query(query);
  }

  getDate(date?){
    let d = date?new Date(date): new Date();
    return `${d.getFullYear()}-${d.getMonth()+1}-${d.getDate()}`;
  }
 

  
}
