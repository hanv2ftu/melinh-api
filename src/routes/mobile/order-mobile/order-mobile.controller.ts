import { Controller, Get, Post, Body, Put, Param, Delete, UseGuards, Req, Query, HttpCode, HttpStatus } from '@nestjs/common';
import { ApiHeader, ApiOkResponse, ApiTags } from '@nestjs/swagger';
import { JwtAuthGuard } from 'src/common/jwt/jwt-auth.guard';
import { CreateOrderDto } from './dto/create-order.dto';
import { MakeOrderDto } from './dto/make-order.dto';
import {  OrderMobileService } from './order-mobile.service';



@ApiHeader({
  name: 'Authorization',
  description: 'Access token',
})

//Add resource tag for Swagger
@ApiTags('order')
@UseGuards(JwtAuthGuard)
@Controller('order')
export class OrderMobileController {
  constructor(private readonly orderService: OrderMobileService) {}

 
  @HttpCode(HttpStatus.OK)
  @Post()
  create(@Req() request: any, @Body() makeOrderDto: MakeOrderDto) {
    console.log(makeOrderDto)
    return this.orderService.create(makeOrderDto,request.user);
  }
  




  
}
