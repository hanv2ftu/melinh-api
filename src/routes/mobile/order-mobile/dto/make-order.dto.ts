import { Optional } from "@nestjs/common";
import { ApiProperty } from "@nestjs/swagger";
import { IsArray, isArray, IsDefined, IsInt, IsNotEmpty, IsOptional, IsString } from "class-validator";
import { CreateOrderDto } from "./create-order.dto";

export class MakeOrderDto {
    @ApiProperty()
    @IsArray()
    @IsNotEmpty()
    @IsDefined()
    orderList:CreateOrderDto[] ;

    @ApiProperty()
    @IsString()
    @IsNotEmpty()
    @IsDefined()
    addressId: string;

    @Optional()
    deliveryDate: Date;

}
