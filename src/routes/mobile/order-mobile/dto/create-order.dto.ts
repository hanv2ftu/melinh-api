import { Optional } from "@nestjs/common";
import { ApiProperty } from "@nestjs/swagger";
import { IsDefined, IsInt, IsNotEmpty, IsOptional, IsString } from "class-validator";

export class CreateOrderDto {
    @Optional()
    id: string;

    @Optional()
    userId: string;

    @Optional()
    phone: string;

    @Optional()
    addressId: string;

    @Optional()
    status: string;

    @Optional()
    deliveryDate: Date;

    @ApiProperty()
    @IsString()
    @IsNotEmpty()
    @IsDefined()
    flowerId: string;


    @ApiProperty()
    @IsInt()
    @IsNotEmpty()
    @IsDefined()
    quantity: number;


    @Optional()
    createdDate: Date;

    @Optional()
    createdBy: string;


    @Optional()
    updatedDate: Date;

    @Optional()
    updatedBy: string;
}
