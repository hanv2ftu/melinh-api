import { Connection } from 'typeorm';

import { Constants } from "src/common/constants";
import { Order } from './entities/order.entity';


export const OrderMobileProviders = [
    {
        provide: Constants.orderMobileReposistory,
        useFactory: (connection: Connection) => connection.getRepository(Order),
        inject: [Constants.databaseConnection],
    },
];
