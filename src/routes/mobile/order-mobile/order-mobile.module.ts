import { Module } from '@nestjs/common';
import { DatabaseModule } from 'src/common/database/database.module';
import { OrderMobileController } from './order-mobile.controller';
import { OrderMobileProviders } from './order-mobile.providers';
import { OrderMobileService } from './order-mobile.service';

@Module({
    imports: [DatabaseModule],
    controllers: [OrderMobileController],
    providers: [...OrderMobileProviders,OrderMobileService],
    exports:[OrderMobileService]
})
export class OrderMobileModule {}
