import { Controller, Get, Post, Body, Put, Param, Delete, UseGuards, Req, Query, HttpCode, HttpStatus } from '@nestjs/common';
import { ApiHeader, ApiTags } from '@nestjs/swagger';
import {   PromotionService } from './promotion.service';





//Add resource tag for Swagger
@ApiTags('promotion')
@Controller('promotion')
export class PromotionController {
  constructor(private readonly promotionService: PromotionService) {}


  @HttpCode(HttpStatus.OK)
  @Get()
  findAll(@Req() request: any) {
    return this.promotionService.findAll();
  }


}
