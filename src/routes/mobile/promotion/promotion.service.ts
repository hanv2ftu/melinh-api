import { Inject, Injectable } from '@nestjs/common';
import { Constants } from 'src/common/constants';
import { DeleteResult, getManager, Repository } from 'typeorm';


import { v4 as uuidv4 } from 'uuid';
import { Promotion } from './entities/promotion.entity';


@Injectable()
export class PromotionService {
  constructor(@Inject(Constants.promotionReposistory)
  private promotionRepository: Repository<Promotion>,
  ) {

  }

 

  async findAll(): Promise<any> {
   

    const manager = getManager();
    const totalDetails = await manager.createQueryBuilder(Promotion, "promotion")
      .select("count(promotion.id) as total")
      .getRawOne();
    const list = await manager.createQueryBuilder(Promotion, "promotion")
      .select("promotion.id", "id")
      .addSelect("promotion.descripition", "descripition")
      .getRawMany();
    return { total: parseInt(totalDetails.total), list: list };
  }

  async findOne(id: string): Promise<Promotion> {
    return await this.promotionRepository.findOne(id);
  }



  async remove(id: string): Promise<DeleteResult> {
    return await this.promotionRepository.delete(id);
  }
  
  
}
