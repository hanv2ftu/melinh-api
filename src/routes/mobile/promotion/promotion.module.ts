import { Module } from '@nestjs/common';
import { DatabaseModule } from 'src/common/database/database.module';
import { PromotionProviders } from './promotion.providers';
import { PromotionService } from './promotion.service';
import { PromotionController } from './promotioncontroller';

@Module({
    imports: [DatabaseModule],
    controllers: [PromotionController],
    providers: [...PromotionProviders,PromotionService],
    exports:[PromotionService]
})
export class PromotionModule {}
