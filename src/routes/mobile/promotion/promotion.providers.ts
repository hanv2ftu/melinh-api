import { Connection } from 'typeorm';

import { Constants } from "src/common/constants";
import { Promotion } from './entities/promotion.entity';



export const PromotionProviders = [
    {
        provide: Constants.promotionReposistory,
        useFactory: (connection: Connection) => connection.getRepository(Promotion),
        inject: [Constants.databaseConnection],
    },
];
