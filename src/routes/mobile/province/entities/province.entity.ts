import { Entity, Column, PrimaryGeneratedColumn, BeforeInsert, ManyToOne, Unique, JoinColumn, OneToOne } from 'typeorm';
import * as bcrypt from 'bcrypt';
import { MobileUser } from '../../auth-mobile/entities/userMobile.entity';


@Entity({ name: 'Province' })
export class Province {
    @PrimaryGeneratedColumn()
    id: string;

    @Column({ type: "varchar", length: 100 })
    code: string;

    @Column({ type: "varchar", length: 100 })
    province: string;


   
}
