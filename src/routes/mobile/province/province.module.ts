import { Module } from '@nestjs/common';
import { DatabaseModule } from 'src/common/database/database.module';
import { ProvinceProviders } from './province.providers';
import { ProvinceService } from './province.service';
import { ProvinceController } from './provincecontroller';

@Module({
    imports: [DatabaseModule],
    controllers: [ProvinceController],
    providers: [...ProvinceProviders,ProvinceService],
    exports:[ProvinceService]
})
export class ProvinceModule {}
