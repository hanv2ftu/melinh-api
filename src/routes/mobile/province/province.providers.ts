import { Connection } from 'typeorm';

import { Constants } from "src/common/constants";
import { Province } from './entities/province.entity';


export const ProvinceProviders = [
    {
        provide: Constants.provinceReposistory,
        useFactory: (connection: Connection) => connection.getRepository(Province),
        inject: [Constants.databaseConnection],
    },
];
