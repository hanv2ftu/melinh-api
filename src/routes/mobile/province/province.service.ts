import { Inject, Injectable } from '@nestjs/common';
import { Constants } from 'src/common/constants';
import { DeleteResult, getManager, Repository } from 'typeorm';
import { MobileUser } from '../auth-mobile/entities/userMobile.entity';
import { Province } from './entities/province.entity';
import { v4 as uuidv4 } from 'uuid';

@Injectable()
export class ProvinceService {
  constructor(@Inject(Constants.provinceReposistory)
  private provinceRepository: Repository<Province>) {

  }

 

  async findAll(): Promise<any> {
    const manager = getManager();
    const totalDetails = await manager.createQueryBuilder(Province, "province")
      .select("count(province.id) as total")
      .getRawOne();
    const list = await manager.createQueryBuilder(Province, "province")
      .select("province.id", "id")
      .addSelect("province.code", "code")
      .addSelect("province.province", "province")
      .getRawMany();
    return { total: parseInt(totalDetails.total), list: list };
  }

  
}
