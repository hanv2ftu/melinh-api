import { Controller, Get, Post, Body, Put, Param, Delete, UseGuards, Req, Query, HttpCode, HttpStatus } from '@nestjs/common';
import { ApiHeader, ApiTags } from '@nestjs/swagger';
import { JwtAuthGuard } from 'src/common/jwt/jwt-auth.guard';
import {   ProvinceService } from './province.service';





//Add resource tag for Swagger
@ApiTags('province')
@Controller('province')
export class ProvinceController {
  constructor(private readonly provinceService: ProvinceService) {}

  

  @HttpCode(HttpStatus.OK)
  @Get()
  findAll(@Req() request: any) {
    return this.provinceService.findAll();
  }


}
