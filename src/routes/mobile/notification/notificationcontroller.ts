import { Controller, Get, Post, Body, Put, Param, Delete, UseGuards, Req, Query, HttpCode, HttpStatus } from '@nestjs/common';
import { ApiHeader, ApiTags } from '@nestjs/swagger';
import { JwtAuthGuard } from 'src/common/jwt/jwt-auth.guard';
import {   NotificationService } from './notification.service';





//Add resource tag for Swagger
@ApiTags('notification')
@Controller('notification')
export class NotificationController {
  constructor(private readonly notificationService: NotificationService) {}

  @ApiHeader({
    name: 'Authorization',
    description: 'Access token',
  })
  @HttpCode(HttpStatus.OK)
  @UseGuards(JwtAuthGuard)
  @Get()
  findAll(@Req() request: any) {
    return this.notificationService.findAll(request.user);
  }


}
