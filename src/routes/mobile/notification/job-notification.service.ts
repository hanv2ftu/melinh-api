import { HttpService } from '@nestjs/axios';
import { Inject, Injectable } from '@nestjs/common';
import { Cron } from '@nestjs/schedule';
import { Constants } from 'src/common/constants';
import { Repository } from 'typeorm';
import { AuthMobileService } from '../auth-mobile/auth-mobile.service';
import { NotificationArrayDto } from './dto/notification.array.dto';
import { NotificationService } from './notification.service';
@Injectable()
export class JobNotificationService {
  constructor(@Inject(Constants.notificationReposistory)
  private notificationRepository: Repository<Notification>,
  private authMobileService:AuthMobileService,
  private readonly httpService: HttpService,
  private notificationService : NotificationService
  ) {

  }
  @Cron('0 9 * * *')
  async handleCron() {
    // console.log("test job")
    const result = await this.authMobileService.findAll();
    // console.log(result)
    let array = [];
    result.list.forEach(element => {
      if(element.firebaseId){
        array.push({
          to: element.firebaseId,
          title: "Đặt hoa nhận ngay ưu đãi",
          body: "Giảm ngay 10% cho tất cả các đơn hàng khi đặt qua APP Hoa Tươi Mê Linh từ ngày 15/07/2022 đến 15/08/2022"
        })
      }
    });

    const sendNotifi = await this.httpService.post('https://api.expo.dev/v2/push/send',array).subscribe(async(res)=>{
      // console.log(res)
      let notificationList = [];
      array.forEach(ele=>{
        notificationList.push({
          firebaseId:ele.to,
          title:ele.title,
          descripition:ele.body
        })
      })
      let body:NotificationArrayDto = {
        notificationList:notificationList
      }
      const result = await this.notificationService.create(body);
      // console.log(result)
    },err=>{
      // console.log(err)
    })
    
  }
}
