import { Connection } from 'typeorm';

import { Constants } from "src/common/constants";
import { Notification } from './entities/notification.entity';


export const NotificationProviders = [
    {
        provide: Constants.notificationReposistory,
        useFactory: (connection: Connection) => connection.getRepository(Notification),
        inject: [Constants.databaseConnection],
    },
];
