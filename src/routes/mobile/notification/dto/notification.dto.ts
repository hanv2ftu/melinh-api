import { Optional } from "@nestjs/common";
import { ApiProperty } from "@nestjs/swagger";
import { IsDefined, IsInt, IsNotEmpty, IsOptional, IsString } from "class-validator";

export class NotificationDto {
    @Optional()
    id: string;

    @Optional()
    firebaseId: string;

    @Optional()
    title: string;

    @Optional()
    descripition: string;


    @Optional()
    createdDate: Date;

    @Optional()
    createdBy: string;


    @Optional()
    updatedDate: Date;

    @Optional()
    updatedBy: string;
}
