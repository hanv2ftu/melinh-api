import { Optional } from "@nestjs/common";
import { ApiProperty } from "@nestjs/swagger";
import { IsArray, isArray, IsDefined, IsInt, IsNotEmpty, IsOptional, IsString } from "class-validator";
import { NotificationDto } from "./notification.dto";


export class NotificationArrayDto {
    @ApiProperty()
    @IsArray()
    @IsNotEmpty()
    @IsDefined()
    notificationList:NotificationDto[] ;


}
