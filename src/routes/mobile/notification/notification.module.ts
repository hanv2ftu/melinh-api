import { HttpModule } from '@nestjs/axios';
import {  Module } from '@nestjs/common';
import { DatabaseModule } from 'src/common/database/database.module';
import { AuthMobileModule } from '../auth-mobile/auth-mobile.module';
import { JobNotificationService } from './job-notification.service';
import { NotificationProviders } from './notification.providers';
import { NotificationService } from './notification.service';
import { NotificationController } from './notificationcontroller';

@Module({
    imports: [DatabaseModule,AuthMobileModule,HttpModule],
    controllers: [NotificationController],
    providers: [...NotificationProviders,NotificationService,JobNotificationService],
    exports:[NotificationService]
})
export class NotificationModule {}
