import { Entity, Column, PrimaryGeneratedColumn, BeforeInsert, ManyToOne, Unique, JoinColumn, OneToOne } from 'typeorm';
import * as bcrypt from 'bcrypt';
import { MobileUser } from '../../auth-mobile/entities/userMobile.entity';


@Entity({ name: 'Notification' })
export class Notification {
    @PrimaryGeneratedColumn()
    id: string;

    @Column({ type: "varchar", length: 100 })
    firebaseId: string;

    @Column({ type: "varchar", length: 400 })
    title: string;

    @Column({ type: "varchar", length: 2000 })
    descripition: string;

    @Column({ type: "date" })
    createdDate: Date;


    @Column({ type: "varchar", length: 50 })
    createdBy: string;

    @Column({ type: "date" })
    updatedDate: Date;

    @Column({ type: "varchar", length: 50 })
    updatedBy: string;
}
