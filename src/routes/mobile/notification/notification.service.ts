import { Inject, Injectable } from '@nestjs/common';
import { Constants } from 'src/common/constants';
import { DeleteResult, getManager, Repository } from 'typeorm';
import { MobileUser } from '../auth-mobile/entities/userMobile.entity';
import { Notification } from './entities/notification.entity';
import { v4 as uuidv4 } from 'uuid';
import { AuthMobileService } from '../auth-mobile/auth-mobile.service';
import { NotificationArrayDto } from './dto/notification.array.dto';
import { NotificationDto } from './dto/notification.dto';

@Injectable()
export class NotificationService {
  constructor(@Inject(Constants.notificationReposistory)
  private notificationRepository: Repository<Notification>,
  private authMobileService:AuthMobileService
  ) {

  }

 

  async findAll(user): Promise<any> {
    const users = await this.authMobileService.findOne(user.userId);

    const manager = getManager();
    const totalDetails = await manager.createQueryBuilder(Notification, "notification")
      .select("count(notification.id) as total")
      .where("notification.firebaseId=:firebaseId", { firebaseId: users.firebaseId })
      .getRawOne();
    const list = await manager.createQueryBuilder(Notification, "notification")
      .select("notification.id", "id")
      .addSelect("notification.firebaseId", "firebaseId")
      .addSelect("notification.title", "title")
      .addSelect("notification.descripition", "descripition")
      .where("notification.firebaseId=:firebaseId", { firebaseId: users.firebaseId })
      .orderBy("notification.createdDate",'DESC')
      .limit(10)
      .offset(0)
      .getRawMany();
    return { total: parseInt(totalDetails.total), list: list };
  }
  async findOne(id: string): Promise<Notification> {
    return await this.notificationRepository.findOne(id);
  }



  async remove(id: string): Promise<DeleteResult> {
    return await this.notificationRepository.delete(id);
  }
  
  async create(notificationDto: NotificationArrayDto): Promise<Notification> {
    let array: NotificationDto[] = notificationDto.notificationList;
    let orderArray = array.map((ele:NotificationDto)=>{
      return {
        id:uuidv4(),
        firebaseId:ele.firebaseId,
        title:ele.title,
        descripition:ele.descripition,
        createdDate:this.getDate(),
        createdBy:'ADMIN',
        updatedDate: this.getDate(),
        updatedBy:'ADMIN',
      }
    })
    const manager = getManager();
    var query = "INSERT INTO melinh.Notification(id, firebaseId, title, descripition, createdDate, createdBy, updatedDate, updatedBy) VALUES "
    orderArray.forEach((c, idx) => {
      query += "('" + c.id + "','" + c.firebaseId + "','" + c.title + "','" + c.descripition + "','"  + c.createdDate + "','" + c.createdBy + "','" + c.updatedDate + "','" + c.updatedBy + "')";
      if (idx < orderArray.length - 1) {
        query += " , "
      }
    });
    console.log(query);
    return await manager.query(query);
  }

  getDate(date?){
    let d = date?new Date(date): new Date();
    return `${d.getFullYear()}-${d.getMonth()+1}-${d.getDate()}`;
  }
}
