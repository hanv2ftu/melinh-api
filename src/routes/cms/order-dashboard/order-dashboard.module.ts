import { Module } from '@nestjs/common';
import { DatabaseModule } from 'src/common/database/database.module';
import { OrderDashboardController } from './order-dashboard.controller';
import { OrderDashboardProviders } from './order-dashboard.providers';
import { OrderDashboardService } from './order-dashboard.service';

@Module({
    imports: [DatabaseModule],
    controllers: [OrderDashboardController],
    providers: [...OrderDashboardProviders,OrderDashboardService],
    exports:[OrderDashboardService]
})
export class OrderDashboardModule {}
