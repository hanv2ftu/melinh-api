import { Inject, Injectable } from '@nestjs/common';
import { Constants } from 'src/common/constants';
import { DeleteResult, getManager, Repository } from 'typeorm';
import { Order } from './entities/order.entity';
import { v4 as uuidv4 } from 'uuid';
import { GetListOrderDto } from './dto/get-list-order.dto';
import { Flower } from '../flower-dashboard/entities/flower.entity';
import { MobileUser } from 'src/routes/mobile/auth-mobile/entities/userMobile.entity';
import { Address } from 'src/routes/mobile/address/entities/address.entity';
import { UpdateOrderDto } from './dto/update-order.dto';
import { Province } from 'src/routes/mobile/province/entities/province.entity';



@Injectable()
export class OrderDashboardService {
  constructor(@Inject(Constants.orderDashboardReposistory)
  private orderRepository: Repository<Order>) {

  }
  async findAll(getListOrderDto: GetListOrderDto): Promise<any> {

    const manager = getManager();
    const totalDetails = await manager.createQueryBuilder(Order, "Order")
      .select("count(Order.id) as total")
      .getRawOne();
    const list = await manager.createQueryBuilder(Order, "Order")
      .select("Order.id", "id")
      .addSelect("Order.quantity", "quantity")
      .addSelect("Order.status", "status")
      .addSelect("Order.deliveryDate", "deliveryDate")
      .addSelect("Order.createdDate", "createdDate")
      .addSelect("Order.createdBy", "createdBy")
      .addSelect("Order.updatedDate", "updatedDate")
      .addSelect("flower.id", "flowerId")
      .addSelect("flower.title", "title")
      .addSelect("flower.avaUrl", "avaUrl")
      .addSelect("flower.imageList", "imageList")
      .addSelect("flower.currentPrice", "currentPrice")
      .addSelect("flower.oldPrice", "oldPrice")
      .addSelect("flower.category", "category")
      .addSelect("flower.description", "description")
      .addSelect("flower.status", "statusFlower")
      .addSelect("MobileUser.id", "userId")
      .addSelect("MobileUser.fullName", "orderName")
      .addSelect("MobileUser.phone", "orderPhone")
      .addSelect("MobileUser.address", "orderAddress")
      .addSelect("Address.id", "addressId")
      .addSelect("Address.fullName", "recieverName")
      .addSelect("Address.phone", "recieverPhone")
      .addSelect("Address.address", "recieverAddress")
      .addSelect("province.id", "provinceId")
      .addSelect("province.code", "provinceCode")
      .addSelect("province.province", "province")
      .leftJoin(Flower, "flower", "flower.id=Order.flowerId")
      .leftJoin(MobileUser, "MobileUser", "MobileUser.id=Order.userId")
      .leftJoin(Address, "Address", "Address.id=Order.addressId")
      .leftJoin(Province, "province", "province.id=Address.provinceId")
      .getRawMany();
    return { total: parseInt(totalDetails.total), list: list };
  }

  getDate(date?){
    let d = date?new Date(date): new Date();
    return `${d.getFullYear()}-${d.getMonth()+1}-${d.getDate()}`;
  }
 
  async findOne(id: string): Promise<Order> {
    return await this.orderRepository.findOne(id);
  }

  async update(id: string, updateOrderDto: UpdateOrderDto): Promise<Order> {
    await this.orderRepository.update(id, updateOrderDto);
    return await this.orderRepository.findOne(id);
  }

  async remove(id: string): Promise<DeleteResult> {
    return await this.orderRepository.delete(id);
  }
  
}
