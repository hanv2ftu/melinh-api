import { Controller, Get, Post, Body, Put, Param, Delete, UseGuards, Req, Query, HttpCode, HttpStatus } from '@nestjs/common';
import { ApiHeader, ApiOkResponse, ApiTags } from '@nestjs/swagger';
import { JwtAuthGuard } from 'src/common/jwt/jwt-auth.guard';
import { GetListOrderDto } from './dto/get-list-order.dto';
import { UpdateOrderDto } from './dto/update-order.dto';
import {  OrderDashboardService } from './order-dashboard.service';



@ApiHeader({
  name: 'Authorization',
  description: 'Access token',
})

//Add resource tag for Swagger
@ApiTags('order')
@UseGuards(JwtAuthGuard)
@Controller('order')
export class OrderDashboardController {
  constructor(private readonly orderService: OrderDashboardService) {}

 
  @HttpCode(HttpStatus.OK)
  @ApiOkResponse({ description: 'Successfully getlist' })
  @Post('getlist')
  findAll(@Req() request: any, @Body() getListOrderDto: GetListOrderDto) {
    return this.orderService.findAll(getListOrderDto);
  }

  @HttpCode(HttpStatus.OK)
  @Put(':id')
  update(@Param('id') id: string, @Body() updateOrderDto: UpdateOrderDto) {
    return this.orderService.update(id, updateOrderDto);
  }

  
  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.orderService.remove(id);
  }


  
}
