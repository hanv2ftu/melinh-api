import { Entity, Column, PrimaryGeneratedColumn, BeforeInsert, ManyToOne, Unique, JoinColumn, OneToOne } from 'typeorm';




@Entity({ name: 'Order' })
export class Order {
    @PrimaryGeneratedColumn()
    id: string;


    @Column({ type: "varchar", length: 100 })
    flowerId: string;

    @Column({ type: "varchar", length: 100 })
    userId: string;

    @Column({ type: "varchar", length: 50 })
    phone: string;

    @Column({ type: "varchar", length: 100 })
    addressId: string;

    @Column({ type: "integer" })
    quantity: number;

    @Column({ type: "varchar", length: 50 })
    status: string;


    @Column({ type: "date" })
    deliveryDate: Date;

    @Column({ type: "date" })
    createdDate: Date;


    @Column({ type: "varchar", length: 50 })
    createdBy: string;

    @Column({ type: "date" })
    updatedDate: Date;

    @Column({ type: "varchar", length: 50 })
    updatedBy: string;
}
