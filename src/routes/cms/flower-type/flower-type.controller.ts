import { Controller, Get, Post, Body, Put, Param, Delete, UseGuards, Req, Query, HttpCode, HttpStatus } from '@nestjs/common';
import { ApiHeader, ApiOkResponse, ApiTags } from '@nestjs/swagger';
import { JwtAuthGuard } from 'src/common/jwt/jwt-auth.guard';
import {  FlowerTypeService } from './flower-type.service';
import { GetFlowerTypeDto } from './dto/get-flower-type.dto';



@ApiHeader({
  name: 'Authorization',
  description: 'Access token',
})

//Add resource tag for Swagger
@ApiTags('flower-type')
@UseGuards(JwtAuthGuard)
@Controller('flower-type')
export class FlowerTypeController {
  constructor(private readonly flowerService: FlowerTypeService,) {}
  
 
  @HttpCode(HttpStatus.OK)
  @ApiOkResponse({ description: 'Successfully getlist' })
  @Post()
  findAll(@Req() request: any, @Body() getFlowerDto: GetFlowerTypeDto) {
    return this.flowerService.findAll(getFlowerDto);
  }

 
 
}
