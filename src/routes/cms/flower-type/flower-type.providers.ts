import { Connection } from 'typeorm';

import { Constants } from "src/common/constants";
import { FlowerType } from './entities/flowerType.entity';


export const FlowerTypeProviders = [
    {
        provide: Constants.flowerTypeReposistory,
        useFactory: (connection: Connection) => connection.getRepository(FlowerType),
        inject: [Constants.databaseConnection],
    },
];
