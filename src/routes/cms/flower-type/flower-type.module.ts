import { Module } from '@nestjs/common';
import { DatabaseModule } from 'src/common/database/database.module';
import { FlowerTypeController } from './flower-type.controller';
import { FlowerTypeProviders } from './flower-type.providers';
import { FlowerTypeService } from './flower-type.service';

@Module({
    imports: [DatabaseModule],
    controllers: [FlowerTypeController],
    providers: [...FlowerTypeProviders,FlowerTypeService],
    exports:[FlowerTypeService]
})
export class FlowerTypeModule {}
