import { Inject, Injectable } from '@nestjs/common';
import { Constants } from 'src/common/constants';
import { DeleteResult, getManager, Repository } from 'typeorm';
import { FlowerType } from './entities/flowerType.entity';
import { v4 as uuidv4 } from 'uuid';
import { Order } from '../order-dashboard/entities/order.entity';
import { GetFlowerTypeDto } from './dto/get-flower-type.dto';

@Injectable()
export class FlowerTypeService {
  constructor(@Inject(Constants.flowerTypeReposistory)
  private flowerRepository: Repository<FlowerType>) {

  }



  async findAll(getFlowerDto: GetFlowerTypeDto): Promise<any> {
    
    const manager = getManager();
    const totalDetails = await manager.createQueryBuilder(FlowerType, "flower")
      .select("count(flower.id) as total")
      .getRawOne();
    const list = await manager.createQueryBuilder(FlowerType, "flower")
      .select("flower.id", "id")
      .addSelect("flower.flowerType", "flowerType")
      .addSelect("flower.descripition", "descripition")
      .getRawMany();
    return { total: parseInt(totalDetails.total), list: list };
  }

 
}
