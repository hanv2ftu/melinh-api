import { Entity, Column, PrimaryGeneratedColumn, BeforeInsert, ManyToOne, Unique, JoinColumn, OneToOne } from 'typeorm';




@Entity({ name: 'FlowerType' })
export class FlowerType {
    @PrimaryGeneratedColumn()
    id: string;

    @Column({ type: "varchar", length: 300 })
    flowerType: string;

    @Column({ type: "varchar", length: 2000 })
    descripition: string;

 
}
